package vn.ngontinh.hatgiongtamhon;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.onesignal.OneSignal;

public class LauncherActivity extends BaseActivity implements IBase, View.OnClickListener {
    private Button btn_read, btn_continued, btn_other;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        getPropertiesFromUI();
        setElementFromLayout();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    public void getPropertiesFromUI() {
        btn_read = findViewById(R.id.btn_read);
        btn_continued = findViewById(R.id.btn_continued);
        btn_other = findViewById(R.id.other_app);
    }

    @Override
    public void setElementFromLayout() {
        btn_other.setOnClickListener(this);
        btn_continued.setOnClickListener(this);
        btn_read.setOnClickListener(this);
        trackScreen("StartApp");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_read:
                Intent i = new Intent(LauncherActivity.this,ListActivity.class);
                startActivity(i);
                Bundle bundle = new Bundle();
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, bundle);
                break;
            case R.id.btn_continued:
                Intent e = new Intent(LauncherActivity.this,ContentActivity.class);
                e.putExtra("page",getSharedPreferences("vitri",0).getInt("vitri",0));
                startActivity(e);
                break;
            case R.id.other_app:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri
                            .parse("market://search?q=pub: Appboost Studio")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri
                            .parse("https://play.google.com/store/search?q=pub: Appboost Studio")));
                }
                break;
        }
    }

}
