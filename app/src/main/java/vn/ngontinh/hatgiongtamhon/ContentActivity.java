package vn.ngontinh.hatgiongtamhon;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.IOException;
import java.io.InputStream;

import vn.ngontinh.hatgiongtamhon.adapter.PagerAdapter;

public class ContentActivity extends BaseActivity implements IBase {

    private ViewPager mViewPager;
    private PagerAdapter adapter;
    private AdView mAdView;
    private AdRequest adRequest;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getPropertiesFromUI();
        setElementFromLayout();
        isContent = true;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
    }

    @Override
    public void getPropertiesFromUI() {
        mViewPager = (ViewPager) findViewById(R.id.vp_pager);
        adRequest = new AdRequest.Builder().build();
        mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        initFullScreenAds();
    }

    @Override
    public void setElementFromLayout() {
        adapter = new PagerAdapter(getSupportFragmentManager(),doc().split("@").length);
        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTitle(doc().split("@")[position]);
                getSharedPreferences("vitri",0).edit().putInt("vitri",position).apply();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        int position = getIntent().getIntExtra("page", 0);
        mViewPager.setCurrentItem(position);
        mViewPager.setOffscreenPageLimit(3);

        trackScreen("Content");

    }
    protected String doc()
    {
        try
        {
            InputStream localInputStream = getAssets().open("tentruyen.txt");
            byte[] arrayOfByte = new byte[localInputStream.available()];
            localInputStream.read(arrayOfByte);
            return new String(arrayOfByte);
        }
        catch (IOException localIOException)
        {
            localIOException.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
