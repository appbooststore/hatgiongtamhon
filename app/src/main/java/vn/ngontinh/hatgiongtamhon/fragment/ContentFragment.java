package vn.ngontinh.hatgiongtamhon.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import vn.ngontinh.hatgiongtamhon.IBase;
import vn.ngontinh.hatgiongtamhon.R;


public class ContentFragment extends Fragment implements IBase {
    private View view;
    private int mCurrentPage;
    private WebView mWebView;
    private ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle localBundle = getArguments();
        this.mCurrentPage = localBundle.getInt("current_page", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_layout, null);
        getPropertiesFromUI();
        setElementFromLayout();
        return view;
    }




    @Override
    public void getPropertiesFromUI() {
        mWebView = (WebView) view.findViewById(R.id.wv_conten);
    }

    @Override
    public void setElementFromLayout() {
        mWebView.loadUrl("file:///android_asset/content/" + this.mCurrentPage + ".html");
    }
}
