package vn.ngontinh.hatgiongtamhon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import vn.ngontinh.hatgiongtamhon.adapter.ListAdapter;

public class ListActivity extends BaseActivity implements IBase {
    private ListView mListView;
    private ListAdapter adapter;
    private String [] items;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        getPropertiesFromUI();
        setElementFromLayout();
    }

    @Override
    public void getPropertiesFromUI() {
        mListView = (ListView) findViewById(R.id.mList);

    }

    @Override
    public void setElementFromLayout() {
        items = doc("tentruyen.txt").split("@");
        adapter = new ListAdapter(ListActivity.this, items);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(ListActivity.this, ContentActivity.class);
                i.putExtra("page",(position));
                startActivity(i);
                finish();
            }
        });
        trackScreen("ListStories");

    }


    protected String doc(String paramString)
    {
        try
        {
            InputStream localInputStream = getAssets().open(paramString);
            byte[] arrayOfByte = new byte[localInputStream.available()];
            localInputStream.read(arrayOfByte);
            String str = new String(arrayOfByte);
            return str;
        }
        catch (IOException localIOException)
        {
            localIOException.printStackTrace();
        }
        return null;
    }
}
