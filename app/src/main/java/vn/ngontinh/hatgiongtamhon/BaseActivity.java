package vn.ngontinh.hatgiongtamhon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class BaseActivity extends AppCompatActivity {
	protected InterstitialAd mInterstitialAd;
	protected AdView mAdView;
	protected AdRequest adRequest;
	protected volatile boolean isContent = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.open_next, R.anim.close_main);
	}

	@Override
	public void onBackPressed() {
		if (mInterstitialAd!=null) {
			if (mInterstitialAd.isLoaded()) {
				mInterstitialAd.show();
				BaseActivity.this.getSharedPreferences("showAds", 0).edit().putInt("showAds", 4).commit();
				return;
			}
		}
		if(isContent){
			Intent intent  = new Intent(BaseActivity.this, ListActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		finish();
		overridePendingTransition(R.anim.open_main, R.anim.close_next);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	protected void trackScreen(String Screen){
	}
	protected void initFullScreenAds(){
		int dem = BaseActivity.this.getSharedPreferences("showAds",0).getInt("showAds",2);
		if (dem!=0){
			BaseActivity.this.getSharedPreferences("showAds",0).edit().putInt("showAds",--dem).apply();
			return;
		}
		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(getString(R.string.fullscreen_ad_unit_id));

		mInterstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {
				super.onAdClosed();
				if(isContent){
					Intent intent  = new Intent(BaseActivity.this, ListActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
				finish();

			}

			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				super.onAdFailedToLoad(errorCode);
			}
		});
		requestNewInterstitial();
	}
	protected void requestNewInterstitial() {
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mInterstitialAd.loadAd(adRequest);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
