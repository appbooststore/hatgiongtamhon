package vn.ngontinh.hatgiongtamhon.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import vn.ngontinh.hatgiongtamhon.R;
import vn.ngontinh.hatgiongtamhon.model.StoryDTO;

/**
 * Created by Phan on 4/20/2016.
 */
public class ListAdapter extends BaseAdapter {
    private Context mContext;
    private  String [] items;
    private TypedArray icon;
    public ListAdapter(Context mContext, String [] items) {
        this.mContext = mContext;
        this.items = items;
        this.icon = mContext.getResources().obtainTypedArray(
                R.array.arr_random);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.item,viewGroup, false);
        }
        ((TextView)view.findViewById(R.id.txt_title)).setText(items[position]);
        ((ImageView)view.findViewById(R.id.img)).setImageResource(icon.getResourceId((position%icon.length()),-1));
        view.startAnimation(AnimationUtils.loadAnimation(this.mContext, R.anim.push_left_in));
        return view;
    }
}
