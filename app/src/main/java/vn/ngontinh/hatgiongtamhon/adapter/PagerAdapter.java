package vn.ngontinh.hatgiongtamhon.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import vn.ngontinh.hatgiongtamhon.fragment.ContentFragment;

public class PagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT;


    public PagerAdapter(FragmentManager fm,  int paramInt2) {
        super(fm);
        this.PAGE_COUNT = paramInt2;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        ContentFragment localMyFragment1 = new ContentFragment();
        Bundle localBundle = new Bundle();
        localBundle.putInt("current_page", position + 1);
        localMyFragment1.setArguments(localBundle);
        return localMyFragment1;
    }

    @Override
    public CharSequence getPageTitle(int paramInt) {
        return paramInt + 1 + "/" + this.PAGE_COUNT;
    }


}
